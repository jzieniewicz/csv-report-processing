import csv
import pycountry
import operator
import datetime

with open('input1.txt', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    file = open('output.csv','w') 
    csv_file.seek(3)    #avoiding first 3 characters
    sortedlist = sorted(csv_reader, key=lambda row:(row['Date'],row['State']), reverse=False) #sorting before modyfication country into code (incorect order in output)
    line_count = 0
    for row in sortedlist:
        
        if line_count == 0:
            line_count += 1   #avoiding header of the file
        file.write(row['Date'].replace("/","-") +', ')  #changing format of the date
        country = row["State"]
        code = pycountry.countries.get(name=country)  #getting ISO code of the countries
        try:
            file.write(code.alpha_3 +', ')
        except:
            file.write('XXX, ')
        file.write(row["Impressions"]+', ')
        numberOfImpressions = row["Impressions"]
        numberOfImpressions = float(numberOfImpressions)
        CTR = row["CTR"].replace("%","")
        CTR = float(CTR)
        numberOfClicks = int(round(CTR * numberOfImpressions, 0))
        numberOfClicks = str(numberOfClicks)
        file.write(numberOfClicks)
        file.write("\r\n") #EOL format - for Windows \n
        line_count += 1
    file.close()

    print(f'Processed {line_count} lines.')
